
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.web.base.TemplateControllerBase;

import java.util.List;


@RequestMapping("/airwothinessstrengthen")
public class AirwothinessstrengthenController extends TemplateControllerBase {


    public void getarticledata(){

        String model = getPara("model");
        String categoryid = getPara("categoryid");
        String tagid = getPara("tagid");
        Integer page = getParaToInt("page",1);
        Integer pageSize = getParaToInt("pageSize", 8);

        String sql="SELECT\n" +
                "\t* \n" +
                "FROM\n" +
                "\t(\n" +
                "\tSELECT\n" +
                "\t\ta.* \n" +
                "\tFROM\n" +
                "\t\tarticle a\n" +
                "\t\tLEFT JOIN article_category_mapping b ON a.id = b.article_id\n" +
                "\t\tLEFT JOIN article_meta_record c ON c.article_id = b.article_id \n" +
                "\tWHERE\n" +
                "\t\tb.category_id = ? \n" +
                "\t\tAND c.\n" +
                "\tVALUE\n" +
                "\t\t= ? \n" +
                "\t) sj\n" +
                "\tLEFT JOIN article_category_mapping acm ON acm.article_id = sj.id \n" +
                "WHERE\n" +
                "\tacm.category_id = ? and rownum <=8";

        System.out.println(sql);

        List<Record> records = Db.find(sql
            ,categoryid
            ,model
            ,tagid
        );
        renderJson("articledata", records);

    }

}
