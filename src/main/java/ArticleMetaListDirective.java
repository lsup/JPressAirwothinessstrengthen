import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

/**
 * 文章原信息增强
 */
@JFinalDirective("articleMetaList")
public class ArticleMetaListDirective extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Integer id = getParaToInt("id", scope);
        Record record = Db.findById("article_meta_info",id);
        String value = record.getStr("value");
        String[] values = value.split(",");
        scope.setLocal("values",values);
        scope.setLocal("record",record);
        renderBody(env, scope, writer);

    }

    @Override
    public boolean hasEnd() {
        return true;
    }

}
